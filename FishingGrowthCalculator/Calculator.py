import os
from datetime import datetime

from Player import Player

# File
dataFile = "Data/FishingGrowth.csv"

# Internal memory
dateList = []
playerList = []


def uintTo2DigitString(n):
    if n < 10:
        return "0" + str(n)
    else:
        return str(n)


def loadData():
    # Check file exist
    if not os.path.exists(dataFile):
        os.makedirs(os.path.dirname(dataFile), exist_ok=True)
        saveData()

    # Clear internal memory
    global dateList
    global playerList
    dateList = []
    playerList = []

    # Load data file
    f = open(dataFile, "r")

    count = 0
    for line in f:
        # Trim \n from end of line
        line = line.rstrip().rstrip(',')

        if count <= 0:
            # First line
            dateList = line.split(",")[1:]
        else:
            # Read data
            lineSplit = line.split(",")
            playerList.append(Player(lineSplit[0], lineSplit[1:]))

        count += 1
    
    f.close()


def saveData():
    f = open(dataFile, "w")
    
    # Write first line
    f.write("Name,")
    for date in dateList:
        f.write(date + ",")
    f.write("\n")

    # Write players
    for p in playerList:
        f.write(p.ToString() + "\n")
    
    print("Saved!")

    f.close()


def addData():
    # Personal new score
    myNewScore = int(input("Enter your current score: "))

    # Check cancel
    if (cancelOrExit(myNewScore)):
        return False

    # Players' new scores
    for p in playerList:
        newScore = int(input("Enter " + p.Name + "'s new score: "))

        # Check cancel
        if (cancelOrExit(newScore)):
            for p2 in playerList:
                if p2 != p:
                    p2.RemoveLast()
                else:
                    return False

        # Add entry into player
        p.AddEntry(myNewScore, newScore)

    # Add new date
    dt = datetime.now()
    dateList.append(str(dt.day) + "/" + str(dt.month) + "/" + str(dt.year) + " " + uintTo2DigitString(dt.hour) + uintTo2DigitString(dt.minute))

    # Save
    saveData()
    return True


def addPlayer():
    while True:
        name = input("Enter player name: ")

        # Check cancel
        if cancelOrExit(name):
            break

        # Check if there are existing players, load the same size
        initSize = 0
        if len(playerList) > 0:
            initSize = len(playerList[0].Diff)
        
        # Add player
        playerList.append(Player(name, [], initSize))

    # Save
    saveData()


def removePlayer():
    while True:
        name = input("Enter player name: ")

        # Check cancel
        if cancelOrExit(name):
            break

        # Remove player
        toRemove = None
        for p in playerList:
            if name == p.Name:
                toRemove = p
                break
        if toRemove:
            playerList.remove(toRemove)
            print(name + " has been removed.")
        else:
            print (name + " does not exists.")

    # Save
    saveData()


def removeLast():
    dateList.pop()
    for p in playerList:
        p.RemoveLast()

    # Save
    saveData()


def clearData():
    if os.path.exists(dataFile):
        os.remove(dataFile)

    # Load fresh
    loadData()


def cancelOrExit(choice):
    if type(choice) == str:
        return True if choice == "0" else False
    elif type(choice) == int:
        return True if choice == 0 else False


def clearCLI():
    os.system("cls")


def run():
    cli = []
    cli.append("(1) Add players")
    cli.append("(2) Add data")
    cli.append("(3) Remove last data")
    cli.append("(4) Remove players")
    cli.append("(5) Clear data")
    cli.append("(0) Exit")

    while True:
        for ui in cli:
            print(ui)
        choice = int(input("Enter your choice: "))

        if choice < 0 or choice >= len(cli):
            # Invalid choice
            clearCLI()
            print("Invalid choice entered!")
            continue
        
        # Process choice
        clearCLI()
        if choice == 1:
            addPlayer()
        elif choice == 2:
            addData()
        elif choice == 3:
            removeLast()
        elif choice == 4:
            removePlayer()
        elif choice == 5:
            clearData()
        elif cancelOrExit(choice):
            break



# Main Logic #
loadData()
run()