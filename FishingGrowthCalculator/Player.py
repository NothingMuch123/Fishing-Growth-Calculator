class Player:
    def __init__(self, name, values, initListSize = 0):
        self.Name = name
        self.Diff = []
        self.Growth = []

        # Loop through all values provided
        for v in values:
            if not v:
                continue
            vSplit = v.split(" ")
            self.Diff.append(int(vSplit[0]))
            self.Growth.append(vSplit[1][1:-1])

        # Add empty entries if existing entries available on other players
        self.AddEmpty(initListSize)


    def ToString(self):
        # Add name
        result = self.Name + ","

        # Add values
        for i in range(len(self.Diff)):
            result += str(self.Diff[i]) + " (" + self.Growth[i] + "),"

        # Return final string
        return result


    def AddEntry(self, myNewScore, newScore):
        if len(self.Diff) <= 0:
            self.Diff.append(newScore - myNewScore)
            self.Growth.append("+0%")
        else:
            lastDiff = self.Diff[-1]
            newDiff = newScore - myNewScore

            # Write new diff
            self.Diff.append(newDiff)

            # Stop divide by zero error for growth calculation if no last diff information
            diffDiff = lastDiff - newDiff
            if lastDiff == 0:
                self.Growth.append("+" + str(diffDiff) + "%" if diffDiff >= 0 else str(diffDiff) + "%")
                return

            # Write new growth
            if diffDiff >= 0:
                # Positive growth
                self.Growth.append("+" + str(round(100 / lastDiff * diffDiff, 2)) + "%")
            else:
                # Negative growth
                self.Growth.append(str(round(100 / lastDiff * diffDiff, 2)) + "%")


    def AddEmpty(self, cap = 1):
        length = len(self.Diff)
        if (cap <= length):
            return
        for i in range(cap - length):
            self.Diff.append(0)
            self.Growth.append("+0%")


    def RemoveLast(self):
        if (len(self.Diff) <= 0):
            return

        self.Diff.pop()
        self.Growth.pop()