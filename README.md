# Fishing Growth Calculator

Fishing Growth Calculator for the mobile game Moonlight Sculptor to aid in climbing ranks.


Setup:
1. Install Python 3.8 (Python 3.8 was used for this development)
2. Run Setup.bat once to initialise virtual environment


How to use:
1. Run Start.bat to start calculator
2. Add player names, (Type "0" to stop adding)
3. Add data of each player entered (Type "0" to cancel adding)
4. View results in FishingGrowth.csv in Data folder


Excel column-width adjustment for easy viewing
1. Highlight the entire data table using Ctrl + A
2. Click Format -> AutoFit Column Width
3. Close without saving